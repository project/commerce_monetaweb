<?php

/**
 * @file
 * commerce_monetaweb.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_monetaweb_default_rules_configuration() {
  $items = array();
  $items['rules_commerce_monetaweb_checkout_completed'] = entity_import('rules_config', '{ "rules_commerce_monetaweb_checkout_completed" : {
      "LABEL" : "Commerce Monetaweb Checkout Completed",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "10",
      "ACTIVE" : false,
      "TAGS" : [ "Commerce Monetaweb Setefi" ],
      "REQUIRES" : [ "commerce_payment", "commerce_order", "commerce_checkout" ],
      "ON" : [ "commerce_checkout_complete" ],
      "IF" : [
        { "commerce_payment_order_balance_comparison" : { "commerce_order" : [ "commerce_order" ], "value" : "0" } },
        { "commerce_payment_selected_payment_method" : {
            "commerce_order" : [ "commerce_order" ],
            "method_id" : "commerce_monetaweb"
          }
        }
      ],
      "DO" : [
        { "commerce_order_update_status" : { "commerce_order" : [ "commerce_order" ], "order_status" : "completed" } }
      ]
    }
  }');

  return $items;
}
