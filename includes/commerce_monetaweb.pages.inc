<?php

/**
 * responseToMerchantUrl callback
 */
function commerce_monetaweb_notify($order_id, $redirect_key) {
  if (empty($_POST)) {
    drupal_access_denied();
    drupal_exit();
  }

  $paymentID = $_POST['paymentid'];
  $securityToken = $_POST["securitytoken"];

  $conditions = array('paymentid' => $paymentID, 'securitytoken' => $securityToken);
  $session = commerce_monetaweb_session_load_by_criteria($conditions);

  $response = array(
    'authorizationcode' => $_POST['authorizationcode'],
    'customfield' => $_POST["customfield"],
    'merchantorderid' => $_POST['merchantorderid'],
    'responsecode' => $_POST['responsecode'],
    'result' => $_POST['result'],
    'rrn' => $_POST['rrn'],
    'threedsecure' => $_POST['threedsecure'],
    'changed' => REQUEST_TIME
  );

  $record = array_merge($session, $response);
  drupal_write_record('commerce_monetaweb_sessions', $record, 'cmsid');

  $reply = url('checkout/' . $order_id . '/payment/return/' . $redirect_key, array('query' => array('paymentid' => $paymentID), 'absolute' => TRUE));

  print $reply;
  drupal_exit();
}